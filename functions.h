/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   functions.h
 * Author: flavi
 *
 * Created on 17 de Dezembro de 2019, 18:22
 */

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>   

#define TAM 50
#define TAM_INICIAL 10
#define TAM_INCREMENTO 10
#define TAM_STR 20 
#define TAM_DECREMENTO 3 
#define PESO_MAX 20
#define VOLUME_MAX 0.125


    /*
     * 
     */
    typedef struct Artigo artigo;
    typedef struct Pessoa pessoa;
    typedef struct Encomenda encomenda;
    typedef struct Morada morada;
    typedef struct Diretor diretor;
    typedef struct Volume Volume;
    typedef struct cop_Postal_Correspondencia topCodPostal;
    
    enum encomenda_estado {
        expedida, concluida, nao_expedida, cancelada
    };

    enum tipo_transporte {
        regular, urgente, volumosa, fragil, pesada, custo_km
    };

    enum tipo_entrega {
        Urgente, Serv_Regular
    };

    enum Regiao {
        I, C
    };

    enum Tipo_Utilizador {
        diret, clit
    };

    enum Estado {
        ativo, inativo
    };

    struct Volume {
        float altura;
        float comprimento;
        float largura;
    };

    struct Artigo {
        int id_artigo;
        char designacao[50];
        int tratamento;
        float peso;
        Volume volume;
    };

    struct Morada {
        int numero;
        char endereco[150];
        char cidade[30];
        int cod_postal;
    };

    struct Pessoa {
        enum Tipo_Utilizador user;
        int nif;
        int cc;
        char nome[50];
        morada morad;
        enum Estado estado;
        int qt_enc;
    };

    struct Encomenda {
        int id;
        pessoa* client;
        enum tipo_entrega entrega;
        enum encomenda_estado estado;
        int origem;
        int destino;
        float kms;
        artigo *artigos;
        int num_artigos;
        float custo_transporte;
        struct tm data;
        int mes;


    };
    
    struct cop_Postal_Correspondencia{
        int id;
        int codPostal;
        int conta;
    };


    int verifica_utilizador(int, pessoa*, int);

    void ReadString(char*, int, char []);

    void ReadInt(int*, int, int, char[]);

    int redimensiona(pessoa **cliente, int*);

    void introduzir_cliente(int*, pessoa*, int*,int);
    
    void altera_cliente(pessoa* arr);
    
    void apagar_diretor(int *cpess, pessoa *arr);
    
    int apaga_cliente(pessoa *arr);

    void lista_clientes(int, pessoa*);

    void top_clientes(int , pessoa* );
    
    int alocaPessoas(pessoa**, int*);

    
    int saveAll(char*, pessoa*, encomenda*, int, int, float *);

    pessoa* getAtualUser(pessoa*, unsigned int, unsigned int);

    int loadDados(char*, pessoa**, int*, encomenda**, int*, float**);

    void adicionaEncomenda(struct Encomenda**, int *, int *, float *, pessoa *);

    //int leEncomenda(struct Encomenda *encomendas, int pos, int id);

    int pesquisa(struct Encomenda* encomendas, int quantos, int id);

    void listarEncomendas(struct Encomenda* encomendas, int quantos);

    struct Encomenda* getEncomenda(struct Encomenda *encomendas, int quantos, int id);

    void atualizaEncomenda(struct Encomenda *encomendas, int quantos, int id);

    void removeEncomenda(struct Encomenda **encomendas, int *quantos, int *tam_max, int id);

    void cancelarEncomenda(struct Encomenda *encomendas, int quantos, int id);

    float TabelaCusto(float *, enum tipo_transporte);

    float* AlocaTabelaPrecos();

    encomenda* getEncomendasUser(pessoa*, encomenda *, int);

    int gerarFatura(pessoa*, encomenda *, int quantos);

    void reativarUser(pessoa*, int);

    void printTabelaPrecos(float *);

    void editarTabelaPrecos(float *);

    void criarTabelaPrecos(float *);
    
    int alocaEncomendas(encomenda**, int* );

    void adicionaEncomendaV2(struct Encomenda** encomendas, int *conta_enc, int *tam_max, float *TabelaPrecos, pessoa* cliente);


#ifdef __cplusplus
}
#endif

#endif /* FUNCTIONS_H */

