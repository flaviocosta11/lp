/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   functions.c
 * Author: Flavio Pedro's
 *
 * Created on 17 de Dezembro de 2019, 18:22
 */
#include "functions.h"

/**
 * Função necessária para a limpeza do buffer
 */
void cleanInputBuffer() {
    char ch;

    while ((ch = getchar()) != '\n' && ch != EOF);
}

/**
 * Função usada na função ReadInt para verificar se o valor introduzido é um inteiro
 * @param var valor introduzido
 */
void ReadIntVerifica(int *const var) {

    while (scanf("%d", var) != 1) {
        printf("\n Numero inválido ");
        cleanInputBuffer();
    }
    cleanInputBuffer();
}

/**
 * Função que pede um valor inteiro ao utilizador
 * @param var variavel que vai assumir o valor introduzido
 * @param min valor minimo
 * @param max valor maximo
 * @param msg mensagem que vai aparecer ao utilizador
 */
void ReadInt(int *var, int min, int max, char msg[200]) {


    printf("%s ", msg);
    ReadIntVerifica(var);

    while (*var > max || *var < min) {
        printf("\nValor invalido\n");
        printf("%s", msg);
        ReadIntVerifica(var);
    }
}

/**
 * Função usada na função ReadFloat para verificar se o valor introduzido é um float
 * @param var valor introduzido
 */
void readFloatVerifica(float *const var) {

    while (scanf("%f", var) != 1) {
        printf("\n Numero inválido\n ");
        cleanInputBuffer();
    }

}

/**
 * Função que pede um valor do tipo float ao utilizador
 * @param var variavel que vai assumir o valor introduzido
 * @param min valor minimo
 * @param max valor maximo
 * @param msg mensagem que vai ser apresentada ao utilizador
 */
void readFloat(float *var, float min, float max, char msg[200]) {


    printf("%s ", msg);
    readFloatVerifica(var);

    while (*var > max || *var < min) {
        printf("\nValor invalido\n");
        printf("%s", msg);
        readFloatVerifica(var);
        cleanInputBuffer();
    }

    cleanInputBuffer();
}

/**
 * Função que pede uma string ao utilizador
 * @param var variavel que vai assumir a string
 * @param size tamanho maximo da string
 * @param msg mensagem que vai ser apresentada ao utilizador
 */
void ReadString(char *var, int size, char msg[200]) {
    char read[size];
    fflush(stdin);
    printf("%s ", msg);
    scanf("%s", read);
    strcpy(var, read);
}

/**
 * Função responsavel pela realocação de memória
 * @param cliente lista que vai ser realocada
 * @param tam tamanho da lista
 * @return verdadeiro se a realocação for realizada com sucesso
 */
int redimensiona(pessoa **cliente, int *tam) {
    int *temp = NULL;

    temp = (int*) realloc(*cliente, (*tam) * sizeof (int) +TAM_INCREMENTO * sizeof (int)); //o realoc move logo tudo para outro local com mais espaco por isso nao tenho que me preocupar com isso

    if (temp == NULL) {
        printf("\nRealocação da memoria falhou!\n\n");

        return 0;
    } else {
        *tam = (*tam) * sizeof (int) + (*tam) * sizeof (int);
        *cliente = temp;
        return 1;
    }
}

/**
 * Função para introduzir clientes
 * @param apt_ct_pessoa contador de pessoas
 * @param ps lista dos clientes
 * @param tam_maximo maximo de pessoas
 */
void introduzir_cliente(int *apt_ct_pessoa, pessoa *ps, int *tam_maximo, int niff) {

    if (*(apt_ct_pessoa) == (*tam_maximo)) {
        printf("\nentrou\n");
        if (redimensiona(&ps, tam_maximo) == 1) {
            printf("sucess");
        } else {
            printf("erro");
        }
    } else {
        int i = 0;

        pessoa *psss = ps;

        while (i<*apt_ct_pessoa && niff != psss->nif) {
            i++;
            psss++;
        }
        if (i != *apt_ct_pessoa) {
            printf("Esse Cliente ja existe\n\n");

        } else {
            (ps + (*apt_ct_pessoa))->nif = niff;
            ReadString((ps + (*apt_ct_pessoa))->nome, 100, "\nNome:");
            ReadInt(& (ps + (*apt_ct_pessoa))->cc, 10000000, 99999999, "\nCC:");
            ReadString((ps + (*apt_ct_pessoa))->morad.cidade, 100, "\nCidade:");
            ReadInt(& (ps + (*apt_ct_pessoa))->morad.numero, 0, 10000, "\nNumero de porta:");
            ReadString((ps + (*apt_ct_pessoa))->morad.endereco, 100, "\nEndereço:");
            ReadInt(& (ps + (*apt_ct_pessoa))->morad.cod_postal, 1000000, 9999999, "\nCodigo Postal:");

            (ps + (*apt_ct_pessoa))->estado = ativo;
            (ps + (*apt_ct_pessoa))->qt_enc = 0;
            (*apt_ct_pessoa)++;
        }
    }

}

/**
 * Função para o diretor alterar clientes
 * @param cpess contador de pessoas
 * @param arr lista de pessoas
 */
void altera(int *cpess, pessoa *arr) {
    int perg, i = 0, j, esc = 0;
    char nomepesquisa[TAM];

    ReadInt(&perg, 1, 2, "\n1 - Listar todas as pessoas \n2 - Pesquisar pessoas\n");

    switch (perg) {
        case 1:

            while (i < (*cpess)) {
                printf("\n%d - %s \n", i, (arr + i)->nome);
                printf("__________________________________________\n");
                i++;
            }
            int cli_alt = 0;
            ReadInt(&cli_alt, 0, 999999999, "\nEscolha o numero da pessoa que quer alterar\n");

            altera_cliente(arr + cli_alt);
        
                break;

                case 2:
                ReadString(nomepesquisa, 100, "\nIntroduza o nome da pessoa \n");
                i = 0, j = 0;
                while (i <= (*cpess)) {
                    if (i == *cpess && j != 1) {
                        printf("\nNão existe nehuma pessoa com esse nome\n");
                        break;
                    } else if (!strcmp(nomepesquisa, (arr + i)->nome)) {
                        printf("%d - ", i);
                        printf("%s \n", (arr + i)->nome);
                        printf("__________________________________________\n");
                        j = 1;

                    }
                    i++;
                }

                ReadInt(&esc, 0, 99999999, "\nEscolha o numero da pessoa que quer alterar \n");

                altera_cliente(arr + esc);

                break;
                default: printf("erro");
                break;

            }
    }

    /**
     * Função para  editar o perfil dos clientes
     * @param arr lista clientes
     */
    void altera_cliente(pessoa * arr) {

        int op1 = 0, op2 = 0, op3 = 0, op4 = 0, op5 = 0, op6 = 0;

        printf("\n\nIntroduza os novos dados");
        printf("\nNome atual: %s", arr->nome);
        ReadInt(&op1, 0, 1, "\nAlterar nome? 0 - nao 1 - sim ");
        if (op1 == 1) {
            ReadString(arr->nome, 100, "\nNome:");
        }
        printf("\nCC atual: %d", arr->cc);
        ReadInt(&op2, 0, 1, "\nalterar? 0 - nao 1 - sim ");
        if (op2 == 1) {
            ReadInt(&arr->cc, 10000000, 99999999, "\nCC:");
        }

        printf("\nCidade: %s", arr->morad.cidade);
        ReadInt(&op3, 0, 1, "\nalterar? 0 - nao 1 - sim ");
        if (op3 == 1) {
            ReadString(arr->morad.cidade, 100, "\nCidade:");
        }

        printf("\nNºPorta atual: %d", arr->morad.numero);
        ReadInt(&op4, 0, 1, "\nalterar? 0 - nao 1 - sim ");
        if (op4 == 1) {
            ReadInt(&arr->morad.numero, 0, 99999, "\nNº de porta:");
        }

        printf("\nEndereço atual: %s", arr->morad.endereco);

        ReadInt(&op5, 0, 1, "\nalterar? 0 - nao 1 - sim ");
        if (op5 == 1) {
            ReadString(arr->morad.endereco, 100, "\nEndereço:");
        }
        printf("\nCodigo-Postal atual: %d", arr->morad.cod_postal);
        ReadInt(&op6, 0, 1, "\nalterar? 0 - nao 1 - sim ");
        if (op6 == 1) {
            ReadInt(&arr->morad.cod_postal, 1000000, 9999999, "\nCodigo-Postal:");
        }







    }

    /**
     * Função para desativar contas
     * @param arr lista pessoas
     * @return verdadeiro se confirmar a desativação da conta
     */
    int apaga_cliente(pessoa * arr) {
        int res = 0;
        ReadInt(&res, 0, 1, "Tem a certeza que quer desativar a sua conta?\n 0 - não\n 1 - sim");
        if (res == 1) {
            arr->estado = 0;
            return 1;
        } else {
            return 0;

        }
    }
/**
 * Função para o diretor remover clientes 
 * @param cpess contador de pessoas
 * @param arr lista clientes
 */
    void apagar_diretor(int *cpess, pessoa * arr) {
        int perg, i = 0, j, esc = 0;
        char nomepesquisa[TAM];

        ReadInt(&perg, 1, 2, "\n1 - Listar todas as pessoas \n2 - Pesquisar pessoas\n");

        switch (perg) {
            case 1:

                while (i < (*cpess)) {
                    printf("\n%d - %s \n", i, (arr + i)->nome);
                    printf("__________________________________________\n");
                    i++;
                }
                int cli_alt = 0;
                ReadInt(&cli_alt, 0, 999999999, "\nEscolha o numero da pessoa que quer apagar\n");

                while (cli_alt <= *cpess) {
                    arr[cli_alt] = arr[cli_alt + 1];

                    cli_alt++;
                }
                (*cpess)--;
                printf("\nO cliente foi removido");
                break;

            case 2:
                ReadString(nomepesquisa, 100, "\nIntroduza o nome da pessoa \n");
                i = 0, j = 0;
                while (i <= (*cpess)) {
                    if (i == *cpess && j != 1) {
                        printf("\nNão existe nehuma pessoa com esse nome\n");
                        break;
                    } else if (!strcmp(nomepesquisa, (arr + i)->nome)) {
                        printf("%d - ", i);
                        printf("%s \n", (arr + i)->nome);
                        printf("__________________________________________\n");
                        j = 1;

                    }
                    i++;
                }

                ReadInt(&esc, 0, 99999999, "\nEscolha o numero da pessoa que quer apagar \n");

                while (esc <= *cpess) {
                    arr[esc] = arr[esc + 1];

                    esc++;
                }
                (*cpess)--;
                printf("\nO cliente foi removido");
                break;
            default: printf("erro");
                break;

        }

    }

    /**
     * Função para verificar se um nif ja existe
     * @param ctpessoa é o contadorde pessoas
     * @param ps é a lista de pessoas
     * @param nifff é o nif a verificar
     * @return verdadeiro se existir o nif na lista
     */
    int verifica_utilizador(int ctpessoa, pessoa *ps, int nifff) {
        int i = 0;
        pessoa *pes = ps;
        printf("\n verifica_utilizador: antes while : %d \n\n", nifff);
        while (i < ctpessoa) {
            if (pes->nif == nifff) {
                return i;
            }
            pes++;
            i++;
        }

        return -1;
    }

    /**
     * Função para mostrar todos os clientes
     * @param ctpessoa é o contador de pessoas
     * @param ps é a lista de pessoas
     */
    void lista_clientes(int ctpessoa, pessoa * ps) {
        int i = 0;
        pessoa *pes = ps;
        printf("\n");

        while (i < ctpessoa) {
            printf("\nniff: %d nome: %s", pes->nif, pes->nome);
            printf("\n_____________________________________________");
            pes++;
            i++;
        }
        printf("\n");


    }

    /**
     * Função para alterar o estado de um utilizador 
     * @param ps lista de pessoas
     * @param ctpessoas contador de pessoas
     */
    void reativarUser(pessoa *arr, int cpess) {
        int perg, i = 0, j=0, esc = 0;
        char nomepesquisa[TAM];

        ReadInt(&perg, 1, 2, "\n1 - Listar todas as pessoas \n2 - Pesquisar pessoas\n");

        switch (perg) {
            case 1:

                while (i < cpess) {
                    printf("\n%d - %s ", i, (arr + i)->nome);
                    if((arr+i)->estado==0){
                        printf("-(ativo)");
                        
                    }else{
                        printf("-(inativo)");
                    }
                    printf("\n__________________________________________\n");
                    i++;
                }
                int cli_alt = 0;
                ReadInt(&cli_alt, 0, 999999999, "\nEscolha o numero da pessoa para alterar o estado\n");

                int op2 = 0, op3 = 0;
            if(arr->estado==0){
         ReadInt(&op2, 0, 1, "\nO utilizador encontra-se ativo. Pretende desativar? 0 - nao 1 - sim ");
         if(op2==1){
             arr->estado = 1;
         }
 }
         else{
             ReadInt(&op3, 0, 1, "\nO utilizador encontra-se desativado. Pretende ativar? 0 - nao 1 - sim ");
         if(op3==1){
             arr->estado = 0;
         }
                printf("\nO estado docliente foi alterado");
                break;

            case 2:
                ReadString(nomepesquisa, 100, "\nIntroduza o nome da pessoa \n");
                i = 0, j = 0;
                while (i <= cpess) {
                    if (i == cpess && j != 1) {
                        printf("\nNão existe nehuma pessoa com esse nome\n");
                        break;
                    } else if (!strcmp(nomepesquisa, (arr + i)->nome)) {
                        printf("%d - ", i);
                        printf("%s ", (arr + i)->nome);
                        if((arr+i)->estado==0){
                        printf("-(ativo)");
                        
                    }else{
                        printf("-(inativo)");
                    }
                        printf("\n__________________________________________\n");
                        j = 1;

                    }
                    i++;
                }

                ReadInt(&esc, 0, 99999999, "\nEscolha o numero da pessoa que quer apagar \n");

               int op = 0, op1 = 0;
            if (arr->estado == 0) {
                ReadInt(&op, 0, 1, "\nO utilizador encontra-se ativo. Pretende desativar? 0 - nao 1 - sim ");
                if (op == 1) {
                    arr->estado = 1;
                }
            }
            else {
                    ReadInt(&op1, 0, 1, "\nO utilizador encontra-se desativado. Pretende ativar? 0 - nao 1 - sim ");
                    if (op1 == 1) {
                        arr->estado = 0;
                    }

                
                printf("\nO estado docliente foi alterado");
                break;
            default: printf("erro");
                break;

        }

    }
        }
    }
    /**
     * Função para listar os 5 clientes com mais encomendas
     * @param cpess contador de pessoas
     * @param arr lista de pessoas
     */
    void top_clientes(int cpess, pessoa *arr){
        int i=0,j=0;
        int top1=0,top2=0,top3=0,top4=0,top5=0;
        int temp4,temp3,temp2,temp1;
        while(i<=cpess){
            while (j<=cpess){
                if(top1< arr[j].qt_enc){
                    temp1=top1;
                    temp2=top2;
                    temp3=top3;
                    temp4=top4;
                    top1=i;
                    top2=temp1;
                    top3=temp2;
                    top4=temp3;
                    top5=temp4;              
                                       
                }
                j++;
            }
            i++;
        }
        printf("Top 5 clientes do mês");
        printf("\nTop 1 - %d Nif: %d", arr[top1].qt_enc , arr[top1].nif);
        printf("\nTop 2 - %d Nif: %d", arr[top2].qt_enc , arr[top2].nif);
        printf("\nTop 3 - %d Nif: %d", arr[top3].qt_enc , arr[top3].nif);
        printf("\nTop 4 - %d Nif: %d", arr[top4].qt_enc , arr[top4].nif);
        printf("\nTop 5 - %d Nif: %d", arr[top5].qt_enc , arr[top5].nif);
    }
    
    
/**
 * Função para obter o numero de encomendas em um determinado ano
 * @param encomendas lista de encomendas
 * @param nEncomendas contador de encomendas
 * @param ano ano que o cliente quer consultar o numero de encomendas
 * @return numero de encomendas para o ano introduzido; -1  se nao existirem encomendas
 */
int getEncTotaisAno(encomenda *encomendas, int nEncomendas, int ano) {

    if (nEncomendas == 0) {
        printf("\nNão existem encomendas!\n");
        return -1;
    } else {

        int i = 0, n_enc = 0;

        for (; i < nEncomendas; i++) {
            if ((encomendas[i].data.tm_year + 1900) == ano) {
                n_enc++;
            }
        }
        return n_enc;
    }
}

//media de encomendas por clinte

/**
 * Função para calcular a média de encomendas por cliente
 * @param pessoas lista pessoas
 * @param nPessoas contador de pessoas
 * @return a media de encomendas por cliente ou -1 se nao existirem clientes
 */
int mediaEncPorCliente(pessoa *pessoas, int nPessoas) {

    if (nPessoas > 0) {
        int i = 0, n_enc = 0, media = 0;

        for (; i < nPessoas; i++) {
            media += pessoas[i].qt_enc;
        }
        media = media / nPessoas;

        return media;
    } else {
        printf("\nErro! Não exitem clientes!\n");
        return -1;
    }

}

/**
 * Função para obter o numero de encomendas totais em um determinado mês
 * @param encomendas lista encomendas
 * @param nEncomendas contador de encomendas 
 * @param mes mês que o utilizador introduziu para consulta
 * @return retorna o numero de encomendas totais para um mes ou -1 se nao existirem encomendas
 */
int getEncTotaisMes(encomenda *encomendas, int nEncomendas, int mes) {

    if (nEncomendas == 0) {
        printf("\nNão existem encomendas!\n");
        return -1;
    } else {
        int i = 0, n_enc = 0;

        for (; i < nEncomendas; i++) {
            if ((encomendas[i].data.tm_mon) == mes) {
                n_enc++;
            }
        }
        return n_enc;
    }
}

/**
 * Função que permite ver o codigo-postal com mais correspondencia
 * @param encomendas apontador para encomendas
 * @param nEncomendas nº de encomendas existentes
 * @return o codpostal com maior correspondencia ou zero se nao existirem encomendas
 */
int getTopCodPostaisDestino(encomenda *encomendas, int nEncomendas) {

    if (nEncomendas == 0) {
        printf("\nNão existem encomendas!\n");
        return 0;
    } else {
        int idAntigoMaior = 0, i = 0, j, tempConta = 0;
        topCodPostal postais[nEncomendas];

        for (i = 0; i < nEncomendas; i++) {
            postais[i].id = encomendas[i].id;
            postais[i].codPostal = encomendas[i].destino;
            postais[i].conta = 1;
            for (j = 0; j < nEncomendas; j++) {
                if (encomendas[j].destino == postais[i].codPostal) {
                    (postais[i].conta)++;
                }
            }
        }
        idAntigoMaior = encomendas[0].id;
        for (i = 1; i < nEncomendas; i++) {
            if (postais[i].conta > postais[i - 1].conta) {
                idAntigoMaior = postais[i].id;
            }
        }

        return encomendas[idAntigoMaior].destino;
    }
}
   
         
            
        
    

    /**
     * Função para alocar memória para a lista pessoas
     * @param pessoas lista pessoas
     * @param tam_max tamanho da lista
     * @return verdadeiro se a realocação foi realizada com sucesso ; falso se a realocação nao foi realizada com sucesso
     */
    int alocaPessoas(pessoa** pessoas, int* tam_max) {

        *pessoas = (pessoa*) malloc(TAM_INICIAL * sizeof (pessoa));

        if (*pessoas == NULL) {
            printf("Erro ao allocar memoria!\n");
            return 0;
        } else {
            *tam_max = TAM_INICIAL;
            return 1;
        }

    }

    /**
     * Função para alocar memória para a lista encomendas
     * @param encomendas é a lista encomenda
     * @param tam_max é o tamanho da lista
     * @return verdadeiro se a realocação foi realizada com sucesso ; falso se a realocação nao foi realizada com sucesso
     */
    int alocaEncomendas(encomenda** encomendas, int* tam_max) {

        *encomendas = (encomenda*) malloc(TAM_INICIAL * sizeof (encomenda));

        if (*encomendas == NULL) {
            printf("Erro ao allocar memoria!\n");
            return 0;
        } else {
            *tam_max = TAM_INICIAL;

            return 1;
        }

    }

    /**
     * Função para guardar todos os dados
     * @param filename nome do ficheiro
     * @param pessoas lista pessoas
     * @param encomendas lista encomendas
     * @param nPessoas contador de pessoas
     * @param nEncomendas contador de encomendas
     * @param TabelaPrecos tabela de preços
     * @return falso se nao conseguir abrir o ficheiro; verdadeiro se os dados foremguardados 
     */
    int saveAll(char* filename, pessoa *pessoas, encomenda *encomendas, int nPessoas, int nEncomendas, float *TabelaPrecos) {

        FILE *file;
        int i;

        if ((file = fopen(filename, "wb")) == NULL) {
            perror("Erro ao abrir o ficheiro.");
            return 0;
        } else {
            fwrite(&nPessoas, sizeof (int), 1, file);
            fwrite(&nEncomendas, sizeof (int), 1, file);
            fwrite(pessoas, sizeof (pessoa), nPessoas, file);
            fwrite(encomendas, sizeof (encomenda), nEncomendas, file);
            fwrite(TabelaPrecos, sizeof (float), 6, file);

            //escrever cada lista de artigos
            for (i = 0; i < nEncomendas; i++) {
                fwrite(encomendas[i].artigos, sizeof (int), encomendas[i].num_artigos, file);
            }

            fclose(file);
            printf("\nDados guardados com Sucesso!\n");
            return 1;
        }
    }

    /**
     * Função para obter o utilizador atual
     * @param pessoas lista de pessoas
     * @param npessoas contadorde pessoas
     * @param nif nif do login
     * @return retorna a pessoa
     */
    pessoa * getAtualUser(pessoa* pessoas, unsigned int npessoas, unsigned int nif) {
        int i;
        for (i = 0; i < npessoas; i++)
            if (pessoas[i].nif == nif) {
                //printf("\n%p\n\n", &pessoas[i]);
                return &pessoas[i];
            }
        return NULL;
    }

    /**
     * Função para obter as encomendas de um utilizador
     * @param pess lista de pessoas
     * @param encomendas lista de encomendas
     * @param mostrarDetalhesEnc 
     * @return as encomendas do utilizador
     */
    encomenda * getEncomendasUser(pessoa* pess, encomenda *encomendas, int mostrarDetalhesEnc) {
        int i = 0;
        encomenda* enc = encomendas;
        encomenda* encs_user;
        printf("getEncomendasUser - NIF: %d", pess->nif);
        printf("\ngetEncomendasUser apontador: %p\n", encomendas);
        printf("\ngetEncomendasUser quantidade encomendas user: %d\n", pess->qt_enc);
        //printf("getEncomendasUser - designacao artigo: %d",encomendas->id);
        for (i = 0; i < (pess->qt_enc); i++) {
            printf("\nentrou\n");
            if ((enc->client->nif) == (pess->nif)) {
                printf("\nEncontrou a %dº encomenda com id: %d ;\n", (i + 1), enc->id);
                encs_user = enc;
                encs_user++;
            }
            enc++;
        }
        printf("\n getEncomendasUser SUCESSO\n");
        return encs_user;
    }

    /**
     * Função para gerar faturas
     * @param pess lista de pessoas
     * @param encomendas lista de encomendas
     * @param quantos quantidade de encomendas
     * @return verdadeiro se a fatura for gerada
     */
    int gerarFatura(pessoa* pess, encomenda *encomendas, int quantos) {
        /*encomenda* encs_user = getEncomendasUser(pess, encomendas, 0);
        printf("chegou gerarFatura");
        printf("\n gerarfatura encomenda: %d",encs_user->id);*/
        //data
        time_t mytime;
        mytime = time(NULL);
        struct tm data_atual = *localtime(&mytime);
        //printf("Data: %d/%d/%d/\n", data_atual.tm_mday, data_atual.tm_mon + 1, data_atual.tm_year + 1900);


        //printf("quantas encomendas tem: %d", pess->qt_enc);
        int i = 0, mes;
        ReadInt(&mes, 1, 12, "Indique o mes");
        while (i < quantos) {
            printf("\nid da conemenda %d \n", encomendas[i].id);
            printf("\nNiff associado %d \n", encomendas[i].client->nif);
            if (encomendas[i].client->nif == pess->nif && encomendas[i].data.tm_mon == mes) {
                printf("encontrou encomenda");
                //fatura
            }
            i++;
        }
        /*
            for (i = 0; i < (pess->qt_enc); i++) {
                printf(" \n entrou\n");
                if (((encomendas[i].client->nif) == (pess->nif)) && ((encomendas[i].data.tm_mon) == data_atual.tm_mon)) {
                    printf("\nEncontrou a %dº encomenda com id: %d ;\n", (i + 1), encomendas[i].id);
                }
            }
         */
        printf("\nDados guardados com Sucesso!\n");

        return 1;
    }

    /*FILE *fp = fopen("ficheiro.pdf", "w");
    char str[] = "fatura em C"; 
    if (fp != NULL) {
        if (fputs(str, fp) < 0) {
            //aconteceu um erro, tratar
        }else{
            printf("\nfatura gerada;\n");
        }
        fclose(fp);
    } else { // aconteceu um erro, tratar 
    }
     */

    /**
     * Função para carregar os dados do ficheiro
     * @param filename nome do ficheiro
     * @param pessoas lista pessoas
     * @param nPessoas contador de pessoas
     * @param encomendas lista de encomendas
     * @param nEncomendas contador de encomendas
     * @param TabelaPrecos tabela de preços
     * @return verdadeiro se conseguir ler os dados
     */
    int loadDados(char* filename, pessoa **pessoas, int *nPessoas, encomenda **encomendas, int *nEncomendas, float **TabelaPrecos) {

        FILE *f;
        int n1 = 0, n2 = 0, i = 0;
        int n3 = 0;

        if ((f = fopen(filename, "rb")) == NULL) {
            perror("Erro ao abrir o ficheiro.");
            *nPessoas = 0;
            *nEncomendas = 0;
            return 0;
        } else {

            //ler inteiros com os tamanhos de cada lista
            n1 = fread(nPessoas, sizeof (int), 1, f);
            n2 = fread(nEncomendas, sizeof (int), 1, f);

            if (n1 != 1 || n2 != 1) {

                printf("Erro ao ler o ficheiro.\n");
                *nPessoas = 0;
                *nEncomendas = 0;
                fclose(f);
                return 0;
            }

            if (*nPessoas != NULL) {
                //printf("ENTROU NAS PESSOAS");

                //alocar lista de Pessoas
                *pessoas = (pessoa*) malloc(*nPessoas * sizeof (pessoa));
                if (*pessoas == NULL) {
                    printf("\n1\n");

                    printf("Erro ao alocar memória.");
                    *nPessoas = 0;
                    *nEncomendas = 0;
                    fclose(f);
                    return 0;
                }

                n1 = fread(*pessoas, sizeof (pessoa), *nPessoas, f);
                printf("\nNIF: %d\n\n", (*pessoas)->nif);
                if (n1 != *nPessoas) {
                    printf("\n2\n");

                    printf("Erro ao ler o ficheiro.\n");
                    *nPessoas = 0;
                    *nEncomendas = 0;
                    free(*pessoas);
                    fclose(f);
                    return 0;
                }
            }


            if (*nEncomendas != NULL) {
                //printf("ENTROU NAS ENCOMENDAS");
                *encomendas = (encomenda*) malloc(*nEncomendas * sizeof (encomenda));

                if (*encomendas == NULL) {
                    printf("Erro ao alocar memória.");
                    *nPessoas = 0;
                    *nEncomendas = 0;
                    free(*pessoas);
                    fclose(f);
                    return 0;
                }

                n1 = fread(*encomendas, sizeof (encomenda), *nEncomendas, f);
                if (n1 != *nEncomendas) {
                    printf("\n4\n");

                    printf("Erro ao ler o ficheiro.\n");
                    printf("Erro ao alocar memória.");
                    *nPessoas = 0;
                    *nEncomendas = 0;
                    free(*pessoas);
                    free(*encomendas);
                    fclose(f);
                    return 0;
                }

                //ler lista de códigos de artigos
                for (i = 0; i<*nEncomendas; i++) {
                    int num_artigos = (*encomendas)[i].num_artigos;

                    (*encomendas)[i].artigos = (artigo*) malloc(num_artigos * sizeof (artigo));
                    if ((*encomendas)[i].artigos == NULL) {
                        printf("Erro ao alocar memória\n");
                        //liberta_memoria(encomendas, nEncomendas, pessoas, nPessoas, i);
                        fclose(f);
                        return 0;
                    }

                    n1 = fread((*encomendas)[i].artigos, sizeof (int), num_artigos, f);
                    if (n1 != num_artigos) {
                        printf("Erro ao ler o ficheiro.\n");
                        //liberta_memoria(encomendas, nEncomendas, pessoas, nPessoas, i);
                        *nPessoas = 0;
                        *nEncomendas = 0;
                        free(*pessoas);
                        free(*encomendas);
                        fclose(f);
                        return 0;
                    }
                }
            }

            *TabelaPrecos = (float*) malloc(6 * sizeof (float));
            n3 = fread(*TabelaPrecos, sizeof (float), 6, f);
            if (n3 != 6) {
                *TabelaPrecos = AlocaTabelaPrecos();
            }

            printf("Dados lidos de ficheiro.\n");

            fclose(f);

            return 1;
        }
    }

    /**
     * Função para os preços das encomendas
     * @param origem origem da encomenda
     * @param dest destino da encomenda
     * @return fator para cálculo do custo em funcao do codigo postal de origem e destino
     */
    float getTabelaFator(int origem, int dest) {

        float tab_precos[9][9] = {
            {0, 2, 3, 4, 5, 6, 7, 8, 9},
            {2, 0.5, 1.4, 1.3, 1.1, 1.0, 0.9, 0.8, 0.7},
            {3, 1.4, 0.5, 2.0, 1.6, 1.45, 2.1, 1.8, 2.2},
            {4, 1.3, 2.0, 0.5, 2.1, 0.78, 2.2, 0.9, 2.5},
            {5, 1.1, 1.6, 2.1, 0.5, 0.8, 0.6, 2.1, 1.6},
            {6, 1.0, 1.45, 0.78, 0.8, 0.5, 0.4, 0.3, 0.2},
            {7, 0.9, 2.1, 2.2, 0.6, 0.4, 0.5, 1.1, 1.3},
            {8, 0.8, 1.8, 0.9, 2.1, 0.3, 1.0, 0.5, 0.7},
            {9, 0.7, 2.2, 2.5, 1.6, 0.2, 1.3, 0.7, 0.5}
        };
        return tab_precos[dest - 1][origem - 1];
    }

    /**
     * Função para alocar memória para a tabela de preços e definir os preços
     * @return a tabela de preços
     */
    float* AlocaTabelaPrecos() {
        //float tabela_precos [6] = {2.85, 7.15, 5, 12, 3, 0.30};
        //printf("%p\n\n",&tabela_precos);
        float *tabela_precos = NULL;
        tabela_precos = (float*) malloc(6 * sizeof (float));


        if (tabela_precos == NULL) {
            printf("Erro ao allocar memoria!\n");
            return 0;
        }
        *tabela_precos = 2.85;
        *(tabela_precos + 1) = 7.15;
        *(tabela_precos + 2) = 5;
        *(tabela_precos + 3) = 12;
        *(tabela_precos + 4) = 3;
        *(tabela_precos + 5) = 0.30;

        return tabela_precos;
    }

    /**
     * Função para mostrar a tabela de preços
     * @param tabela_precos tabela de preços
     */
    void printTabelaPrecos(float *tabela_precos) {
        printf("\nTipo de Transporte | Custo\n");
        printf("____________________________________________________\n");
        printf("Regular            | %.2f €\n", tabela_precos[0]);
        printf("Urgente            | %.2f €\n", tabela_precos[1]);
        printf("Volumosa           | %.2f €\n", tabela_precos[2]);
        printf("Fragil             | %.2f €\n", tabela_precos[3]);
        printf("Pesada             | %.2f €\n", tabela_precos[4]);
        printf("Custo por km       | %.2f €\n", tabela_precos[5]);
    }

    /**
     * Função para editar a tabela de preços
     * @param tabela_precos lista da tabela de preços
     */
    void editarTabelaPrecos(float *tabela_precos) {
        int op;
        float valor;
        printTabelaPrecos(tabela_precos);
        printf("\nQual o Tipo de Transporte que pretende alterar o valor?\nIntroduza o numero da opção que deseja:\n");
        printf("1 (para Regular) | 2 (para Urgente) | 3 (para Volumosa) | \n4 (para Fragil) | 5 (para Pesada) | 6 (para Custo por km)\n");

        ReadInt(&op, 1, 6, "Opcao: ");

        switch (op) {
            case 1:
                readFloat(&valor, 0, 100, "\n(REGULAR) Introduza o novo valor: ");
                *tabela_precos = valor;
                break;
            case 2:
                readFloat(&valor, 0, 100, "\n(Urgente) Introduza o novo valor: ");
                *(tabela_precos + 1) = valor;
                break;
            case 3:
                readFloat(&valor, 0, 100, "\n(Volumosa) Introduza o novo valor: ");
                *(tabela_precos + 2) = valor;
                break;
            case 4:
                readFloat(&valor, 0, 100, "\n(Fragil) Introduza o novo valor: ");
                *(tabela_precos + 3) = valor;
                break;
            case 5:
                readFloat(&valor, 0, 100, "\n(Pesada) Introduza o novo valor: ");
                *(tabela_precos + 4) = valor;
                break;
            case 6:
                readFloat(&valor, 0, 100, "\n(Custo por km) Introduza o novo valor: ");
                *(tabela_precos + 5) = valor;
                break;
        }
        printf("\nOperacao efetuada com sucesso!\n");
    }

    /**
     * Função para criar a tabela de preços
     * @param tabela_precos lista da tabela de preços
     */
    void criarTabelaPrecos(float *tabela_precos) {
        float valor;
        printTabelaPrecos(tabela_precos);

        printf("\n\nIntroduza o respetivo preço para cada tipo de transporte:\n");
        printf("__________________________________________________________________\n");
        readFloat(&valor, 0, 100, "\n(REGULAR) Introduza o valor: ");
        *tabela_precos = valor;

        readFloat(&valor, 0, 100, "\n(Urgente) Introduza o valor: ");
        *(tabela_precos + 1) = valor;

        readFloat(&valor, 0, 100, "\n(Volumosa) Introduza o valor: ");
        *(tabela_precos + 2) = valor;

        readFloat(&valor, 0, 100, "\n(Fragil) Introduza o valor: ");
        *(tabela_precos + 3) = valor;

        readFloat(&valor, 0, 100, "\n(Pesada) Introduza o valor: ");
        *(tabela_precos + 4) = valor;

        readFloat(&valor, 0, 100, "\n(Custo por km) Introduza o valor: ");
        *(tabela_precos + 5) = valor;

        printf("\nOperacao efetuada com sucesso!\n");
    }

    /**
     * Função para calcular o preço do transporte
     * @param precos lista de preços
     * @param transporte tipos de transporte
     * @return o preço do transporte
     */
    float TabelaCusto(float *precos, enum tipo_transporte transporte) {

        if (transporte == fragil) {
            return *(precos + 3);
        } else if (transporte == pesada) {
            return *(precos + 4);
        } else if (transporte == regular) {
            return *(precos);
        } else if (transporte == urgente) {
            printf("czvxvc");

            return *(precos + 1);
        } else if (transporte == volumosa) {
            return *(precos + 2);
        } else if (transporte == custo_km) {
            return *(precos + 5);
        } else {

            return 0;
        }

    }

    /**
     * Função para calcular o tipo de transporte
     * @param precos lista de preços
     * @param enc lista de encomendas
     * @return o custo do transporte
     */
    float calculaTransporte(float *precos, encomenda * enc) {
        //Custo = (Σtipo_transporte + km*preço_km)*fator_cp
        int des, ori, count, count2, i;
        float fator, custo = 0, custo_entrega, peso, volume;
        int tratamento = 0;

        count = log10(enc->origem);
        ori = (enc->origem) / pow(10, count);

        count2 = log10(enc->destino);
        des = (enc->destino) / pow(10, count2); //BUSCAR PRIMEIRO DIGITO DO CODIGO POSTAL

        for (i = 0; i < (enc->num_artigos); i++) {
            peso += enc->artigos[i].peso;
            volume += ((enc->artigos[i].volume.altura)*(enc->artigos[i].volume.comprimento)*(enc->artigos[i].volume.largura));
            if (enc->artigos[i].tratamento == 1) {
                tratamento = 1;
            }
        }

        if (enc->entrega == Urgente) {
            custo_entrega = TabelaCusto(precos, urgente);
        } else {
            custo_entrega = TabelaCusto(precos, regular);
        }

        if (peso > PESO_MAX) {
            custo_entrega += TabelaCusto(precos, pesada);
        }
        if (volume > VOLUME_MAX) {
            custo_entrega += TabelaCusto(precos, volumosa);
        }
        if (tratamento = 1) {
            custo_entrega += TabelaCusto(precos, fragil);
        }

        custo = (custo_entrega + ((enc->kms) * TabelaCusto(precos, custo_km))) * getTabelaFator(ori, des);

        return custo;

    }

    /*********************************************************************************************************************************************************************/

    /**
     * Função para adicionar encomendas
     * @param encomendas lista de encomendas
     * @param conta_enc contador de encomendas
     * @param tam_max tamanho maximo
     * @param TabelaPrecos tabela de preços
     * @param cliente lista de clientes
     */
    void adicionaEncomendaV2(struct Encomenda** encomendas, int *conta_enc, int *tam_max, float *TabelaPrecos, pessoa * cliente) {
        int escolha, nArtigos, i;
        //printf("\nNIF cliente : %d\n", cliente->nif);

        //printf("\n Conta Encomendas : %d\n", *conta_enc);

        printf("\n Tamanho maximno: %d", *tam_max);
        struct Encomenda *novo;

        if ((*conta_enc) == (*tam_max)) {
            printf("atingiu maximo");
            novo = realloc(*encomendas, ((*conta_enc) + TAM_INCREMENTO) * sizeof (struct Encomenda));
            if (novo == NULL) {
                printf("Realocação de memoria falhou, encomendas nao adicionado. \n");
                //return 0;
            } else {
                printf("Realocou memória. Novo tamanho: %d bytes \n", ((*conta_enc) + TAM_INCREMENTO) * sizeof (struct Encomenda));
                *encomendas = novo;
                (*tam_max) += TAM_INCREMENTO;

            }
        }
        (*encomendas)[*conta_enc].client = cliente;
        (*encomendas)[*conta_enc].id = cliente->qt_enc;

        time_t mytime;
        mytime = time(NULL);
        struct tm data_atual = *localtime(&mytime);

        (*encomendas)[*conta_enc].data = data_atual;

        printf("\nData: %d/%d/%d/\n", (*encomendas)[*conta_enc].data.tm_mday, (*encomendas)[*conta_enc].data.tm_mon + 1, (*encomendas)[*conta_enc].data.tm_year + 1900);

        ReadInt(&escolha, 1, 2, "Para escolher o tipo de entrega, prima 1 para entrega urgente ou prima 2 para entrega normal ");
        if (escolha == 1) {
            (*encomendas)[*conta_enc].entrega = Urgente;
        } else if (escolha == 2) {
            (*encomendas)[*conta_enc].entrega = Serv_Regular;
        } else {
            printf("Introduza uma opção valida \n ");
        }
        readFloat(&(*encomendas)[*conta_enc].kms, 0, 20000, "Introduza os kms a percorrer: ");
        //printf("\n\n%f\n\n", (*encomendas)[*conta_enc].kms);

        ReadInt(&(*encomendas)[*conta_enc].origem, 2000000, 9999999, "Introduza o codigo postal origem da encomenda ");
        //printf("\n\n%d\n\n", (*encomendas)[*conta_enc].origem);

        ReadInt(&(*encomendas)[*conta_enc].destino, 2000000, 9999999, "Introduza o codigo postal do destino da encomenda ");
        //printf("\n\n%d\n\n", (*encomendas)[*conta_enc].destino);

        ReadInt(&nArtigos, 1, 999, "Introduza quantos artigos pretende adicionar a encomenda");
        (*encomendas)[*conta_enc].num_artigos = nArtigos;
        //printf("%d", nArtigos); 
        (*encomendas)[*conta_enc].artigos = (artigo*) malloc(nArtigos * sizeof (artigo));
        if ((*encomendas)[*conta_enc].artigos == NULL) {
            printf("Alocação falhou");
        } else {
            for (i = 0; i < nArtigos; i++) {
                readFloat(&(*encomendas)[*conta_enc].artigos[i].peso, 0, 20000, "Introduza o peso em KG: ");
                //printf("\n\n%f\n\n", (*encomendas)[*conta_enc].artigos[i].peso);

                readFloat(&(*encomendas)[*conta_enc].artigos[i].volume.altura, 0, 2000, "Introduza altura em metros: ");
                //printf("\n\n%f\n\n", (*encomendas)[*conta_enc].artigos[i].volume.altura);

                readFloat(&(*encomendas)[*conta_enc].artigos[i].volume.comprimento, 0, 2000, "Introduza o comprimento em metros: ");
                //printf("\n\n%f\n\n", (*encomendas)[*conta_enc].artigos[i].volume.comprimento);

                readFloat(&(*encomendas)[*conta_enc].artigos[i].volume.largura, 0, 2000, "Introduza a largura em metros: ");
                //printf("\n\n%f\n\n", (*encomendas)[*conta_enc].artigos[i].volume.largura);

                ReadInt(&(*encomendas)[*conta_enc].artigos[i].tratamento, 0, 1, "Para escolher se o artigo é fragil, prima 1 para sim ou prima 0 para não: ");
                ReadString((*encomendas)[*conta_enc].artigos[i].designacao, 50, "Designacao do artigo: ");
                (*encomendas)[*conta_enc].artigos[i].id_artigo = i;
                //printf("\n\nid-artigo : %d\n\n", (*encomendas)[*conta_enc].artigos[i].id_artigo);

                if ((i - 1) < nArtigos) {

                    printf("\n\nIntroduza o proximo artigo.\n\n");
                }
            }
        }
        (*encomendas)[*conta_enc].estado = nao_expedida;
        (*encomendas)[*conta_enc].custo_transporte = calculaTransporte(TabelaPrecos, (*encomendas));
        //printf("\n\n%f\n\n", (*encomendas)[*conta_enc].custo_transporte);

        printf("\n___________________________DADOS CLIENTE______________________________________\n");
        printf("\nNIF CLIENTE: %d", (*encomendas)[*conta_enc].client->nif);
        printf("\nmorada CLIENTE: %s", (*encomendas)[*conta_enc].client->morad.endereco);
        printf("\n___________________________FIM DADOS CLIENTE______________________________________\n");

        (cliente->qt_enc)++;
        (*conta_enc)++;

        printf("\n\nEncomenda adicionada \n");

    }

    /*********************************************************************************************************************************************************************/

    /**
     * Função para mostrar encomendas
     * @param encomendas lista de encomendas
     * @param quantos quantidade de encomendas
     */
    void listarEncomendas(struct Encomenda* encomendas, int quantos) {
        int i = 0, j = 0;
        if (quantos == 0) {
            printf("Lista de encomenda vazia.\n");
            printf("\n");
        } else {
            while (i < quantos) {
                printf("Id: %d \n", encomendas->id);
                printf("Mes de criação: %d\n", encomendas->data.tm_mon);
                printf("Nif associado: %d \n", encomendas->client->nif);
                printf("Tipo de entrega: ");
                if (encomendas->entrega == Urgente) {
                    printf("Urgente \n");
                } else {
                    printf("Serviço normal \n");
                }
                printf("Estado da encomenda: ");
                if (encomendas->estado == nao_expedida) {
                    printf("Não expedida \n");
                } else if (encomendas->estado == expedida) {
                    printf("Expedida \n");

                } else if (encomendas->estado == concluida) {
                    printf("Concluida \n");
                } else {
                    printf("Cancelada \n");
                }
                printf("Codigo postal da origem: %d \n", encomendas->origem);
                printf("Codigo postal do destino: %d \n", encomendas->destino);
                printf("Numero de quilometros: %.2f \n", encomendas->kms);
                printf("Numero de artigos: %.0d \n", encomendas->num_artigos);

                while (j < encomendas->num_artigos) {
                    printf("Id do artigo: %d \n", encomendas->artigos[j].id_artigo);
                    printf("Nome do artigo: %s \n", encomendas->artigos[j].designacao);
                    printf("Tipo de tratamento: ");
                    if (encomendas->artigos[j].tratamento == 1) {
                        printf("Frágil \n");
                    } else {
                        printf("Normal \n");
                    }
                    printf("Peso da encomenda: %.2f \n", encomendas->artigos[j].peso);
                    printf("Altura do artigo: %.2f \n", encomendas->artigos[j].volume.altura);
                    printf("Comprimento do artigo: %.2f \n", encomendas->artigos[j].volume.comprimento);
                    printf("Largura do artigo: %.2f \n", encomendas->artigos[j].volume.largura);
                    j++;
                }
                printf("Preço da encomenda %.2f € \n", encomendas->custo_transporte);
                encomendas++;
                i++;
                printf("\n");
                if ((i - 1) < quantos) {
                    printf("\n__________________________________________________________\n");
                }
            }

        }
    }

    /**
     * Função para procurar uma encomenda
     * @param encomendas lista de encomendas
     * @param quantos contador de encomendas
     * @param id id da encomenda
     * @return a posição da encomenda se existir, ou retorna -1 se a encomenda não existir
     */
    int pesquisa(struct Encomenda* encomendas, int quantos, int id) {
        //printf("valor da pesquisa %d \n", id);
        int i = 0;
        while (i < quantos && encomendas[i].id != id)
            i++;
        if (i == quantos) {
            return -1;
        } else {
            return i;
        }
    }

    /**
     * Função para obter uma encomenda
     * @param encomendas lista de encomendas
     * @param quantos contador de encomendas
     * @param id
     * @return a encomenda
     */
    struct Encomenda * getEncomenda(struct Encomenda *encomendas, int quantos, int id) {
        int pos = pesquisa(encomendas, quantos, id);
        if (pos == -1) {
            return NULL;
        } else {

            return &encomendas[pos];
        }
    }

    /**
     * Função para cancelar encomendas
     * @param encomendas lista de encomendas
     * @param quantos contador de encomendas
     * @param id id da encomenda
     */
    void cancelarEncomenda(struct Encomenda *encomendas, int quantos, int id) {
        struct Encomenda *encomenda = getEncomenda(encomendas, quantos, id);

        if (encomenda != NULL) {
            encomenda->estado = cancelada;
            printf("Encomenda cancelada! \n");
        } else {
            printf("Identificador da encomenda nao é válido! \n");
        }
    }

    /**
     * Função para atualizar encomendas
     * @param encomendas lista de encomendas
     * @param quantos contador de encomendas
     * @param id id da encomenda
     */
    void atualizaEncomenda(struct Encomenda *encomendas, int quantos, int id) {
        struct Encomenda *encomenda = getEncomenda(encomendas, quantos, id);
        int escolha, nArtigos, i;
        int a1, a2, a3, a4, a5;
        if (encomenda != NULL) {
            printf("Id: %d \n", encomendas->id);

            /////////////tipo entrega
            printf("Tipo de entrega: ");
            if (encomendas->entrega == Urgente) {
                printf("Urgente, pretende alterar? ");
            } else {
                printf("Serviço normal, pretende alterar? ");
            }
            ReadInt(&a1, 0, 1, "Introduza 1 para alterar ou 0 para manter valor atual");
            if (a1 == 1) {
                ReadInt(&escolha, 1, 2, "Para escolher o tipo de entrega, prima 1 para entrega urgente ou prima 2 para entrega normal ");
                if (escolha == 1) {
                    encomenda->entrega = Urgente;
                } else if (escolha == 2) {
                    encomenda->entrega = Serv_Regular;
                } else {
                    printf("Introduza uma opção valida \n ");
                }
            }

            ///////////km
            printf("Valor atual dos ks é: %f, pretende alterar? ", encomenda->kms);
            ReadInt(&a2, 0, 1, "Introduza 1 para alterar ou 0 para manter valor atual");
            if (a2 == 1) {
                readFloat(&encomenda->kms, 0, 20000, "Introduza os kms a percorrer: ");
            }

            //////////////origem
            printf("O valor atual do codigo postal de origem é: %d, pretende alterar? ", encomendas->origem);
            ReadInt(&a3, 0, 1, "Introduza 1 para alterar ou 0 para manter valor atual");
            if (a3 == 1) {
                ReadInt(&encomenda->origem, 2000000, 6999999, "Introduza o codigo postal origem da encomenda ");
            }

            ///////////destino
            printf("O valor atual do codigo postal do destino é: %d, pretende alterar? ", encomendas->destino);
            ReadInt(&a4, 0, 1, "Introduza 1 para alterar ou 0 para manter valor atual");
            if (a4 == 1) {
                ReadInt(&encomenda->destino, 2000000, 6999999, "Introduza o codigo postal do destino da encomenda ");
            }

            //////////artigos
            printf("Numero atual de artigos na encomenda: %d, pretende alterar? ", encomenda->num_artigos);
            ReadInt(&a5, 0, 1, " Introduza 1 para alterar ou 0 para manter valor atual");
            if (a5 == 1) {
                ReadInt(&nArtigos, 1, 999, "Introduza quantos artigos pretende adicionar a encomenda");
                encomenda->num_artigos = nArtigos;
                encomendas->artigos = (artigo*) malloc(nArtigos * sizeof (artigo));

                if (encomenda->artigos == NULL) {
                    printf("Alocação falhou");
                } else {
                    for (i = 0; i < nArtigos; i++) {
                        readFloat(&encomenda->artigos[i].peso, 0, 20000, "Introduza o peso em KG: ");
                        readFloat(&encomenda->artigos[i].volume.altura, 0, 2000, "Introduza altura em metros: ");
                        readFloat(&encomenda->artigos[i].volume.comprimento, 0, 2000, "Introduza o comprimento em metros: ");
                        readFloat(&encomenda->artigos[i].volume.largura, 0, 2000, "Introduza a largura em metros: ");

                        ReadInt(&encomenda->artigos[i].tratamento, 0, 1, "Para escolher se o artigo é fragil, prima 1 para sim ou prima 0 para não: ");
                        ReadString(encomenda->artigos[i].designacao, 50, "Designacao do artigo: ");
                        encomenda->artigos[i].id_artigo = i;
                        //encomendas[*pos].artigos[i] = id_artigo;
                        if (nArtigos > 1) {
                            printf("\n\nIntroduza o proximo artigo.\n\n");
                        }
                    }
                }
            }
            printf("Encomenda atualizada! \n");
        } else {

            printf("Identificador da encomenda nao é válido! \n");
        }
    }

    /**
     * Função para remover Encomendas
     * @param encomendas lista de encomendas
     * @param quantos conta encomendas
     * @param tam_max tamanho maximo
     * @param id id da encomenda
     */
    void removeEncomenda(struct Encomenda **encomendas, int *quantos, int *tam_max, int id) {
        struct Encomenda *novo;
        int pos;
        pos = pesquisa(*encomendas, *quantos, id);
        if (pos > -1) {
            while (pos < (*quantos) - 1) {
                (*encomendas)[pos] = (*encomendas)[pos + 1];
                pos++;
            }
            (*quantos)--;
            printf("Encomenda removido! \n");
            if (*tam_max - *quantos >= TAM_INCREMENTO) {
                novo = realloc(*encomendas, ((*tam_max) * sizeof (struct Encomenda)) -
                        TAM_DECREMENTO * sizeof (struct Encomenda));
                if (novo == NULL) {
                    printf("Realocação de memória falhou. \n");
                } else {
                    printf("Realocou memória. Novo tamanho %d bytes. \n",
                            ((*tam_max) * sizeof (struct Encomenda)) -
                            TAM_DECREMENTO * sizeof (struct Encomenda));
                    *encomendas = novo;
                    (*tam_max) -= TAM_DECREMENTO;
                }
            }
        } else
            printf("Encomenda com id %d nao existe! \n", id);
    } 