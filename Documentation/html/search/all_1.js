var searchData=
[
  ['c_14',['C',['../functions_8h.html#a08f6ba2c3b81eaa3c79e652afd651625a739ce3f516592d245d16fd8a3893472c',1,'functions.h']]],
  ['calculatransporte_15',['calculaTransporte',['../functions_8c.html#a7d33fa2251691bb822fb8a888e5464ac',1,'functions.c']]],
  ['cancelada_16',['cancelada',['../functions_8h.html#a8b9fd67a5dd45042992d649248fa1dfaa9663ca4fca20a5c9551c870c4513d6ed',1,'functions.h']]],
  ['cancelarencomenda_17',['cancelarEncomenda',['../functions_8c.html#a61deef43c943c2427bed2d71f569e156',1,'cancelarEncomenda(struct Encomenda *encomendas, int quantos, int id):&#160;functions.c'],['../functions_8h.html#a61deef43c943c2427bed2d71f569e156',1,'cancelarEncomenda(struct Encomenda *encomendas, int quantos, int id):&#160;functions.c']]],
  ['cc_18',['cc',['../struct_pessoa.html#a21aaf27a7ac21de349cae5888fb050a3',1,'Pessoa']]],
  ['cidade_19',['cidade',['../struct_morada.html#a0f5ed036afbc4be5aa4886202ef8ce9d',1,'Morada']]],
  ['cleaninputbuffer_20',['cleanInputBuffer',['../functions_8c.html#a11ac4d3ec555747d95fee8ae7aa18b5d',1,'functions.c']]],
  ['client_21',['client',['../struct_encomenda.html#a48d36fa0d4c7b04cea43667e043e9bc9',1,'Encomenda']]],
  ['clit_22',['clit',['../functions_8h.html#a809202e2f691b492476c574d67ba5da1ac75b25cdbd3e48661622615c2603b104',1,'functions.h']]],
  ['cod_5fpostal_23',['cod_postal',['../struct_morada.html#a684692a0ed33a295c82397e3f48f5a03',1,'Morada']]],
  ['comprimento_24',['comprimento',['../struct_volume.html#a0e91296bdeaab641a944e9f21c0b981c',1,'Volume']]],
  ['concluida_25',['concluida',['../functions_8h.html#a8b9fd67a5dd45042992d649248fa1dfaa7ceef70e37f8188d83728fd6dd36be20',1,'functions.h']]],
  ['criartabelaprecos_26',['criarTabelaPrecos',['../functions_8c.html#af40ae6d2dfbbe4342cf5639f3fbedbe8',1,'criarTabelaPrecos(float *tabela_precos):&#160;functions.c'],['../functions_8h.html#a84c074b727fd51cbc593a08a62caabfc',1,'criarTabelaPrecos(float *):&#160;functions.c']]],
  ['custo_5fkm_27',['custo_km',['../functions_8h.html#a814d71ae0f257a0747f8f94fedfbf0cfa5fd34dd148465e8874e47f2a71e6977f',1,'functions.h']]],
  ['custo_5ftransporte_28',['custo_transporte',['../struct_encomenda.html#a8c69029ca265c24ceb4bc59811a2797f',1,'Encomenda']]]
];
