var searchData=
[
  ['editartabelaprecos_34',['editarTabelaPrecos',['../functions_8c.html#a473a8a89d5c689b83a77b56eeb922221',1,'editarTabelaPrecos(float *tabela_precos):&#160;functions.c'],['../functions_8h.html#a691b705ff1c1335c9271da1e5d04b54f',1,'editarTabelaPrecos(float *):&#160;functions.c']]],
  ['encomenda_35',['Encomenda',['../struct_encomenda.html',1,'Encomenda'],['../functions_8h.html#ad8da63286f5210ccb827e1db7474dfd9',1,'encomenda():&#160;functions.h']]],
  ['encomenda_5festado_36',['encomenda_estado',['../functions_8h.html#a8b9fd67a5dd45042992d649248fa1dfa',1,'functions.h']]],
  ['endereco_37',['endereco',['../struct_morada.html#a74a3398c2d09a5c025208414e7139531',1,'Morada']]],
  ['entrega_38',['entrega',['../struct_encomenda.html#af84f7c89018444fa40ae8d5f0bb3a056',1,'Encomenda']]],
  ['estado_39',['estado',['../struct_pessoa.html#ab959d8e590772a3d3181e49393abeb3e',1,'Pessoa::estado()'],['../struct_encomenda.html#a6fdda90322001b6f45847a5e3e566ce6',1,'Encomenda::estado()'],['../functions_8h.html#a9d4630b1e0c310d9b8c562abd8114c49',1,'Estado():&#160;functions.h']]],
  ['expedida_40',['expedida',['../functions_8h.html#a8b9fd67a5dd45042992d649248fa1dfaa688cb46f1899dc6b142211b49dd941f5',1,'functions.h']]]
];
