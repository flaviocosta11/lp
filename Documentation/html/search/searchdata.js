var indexSectionsWithContent =
{
  0: "acdefgiklmnopqrstuv",
  1: "aempv",
  2: "fmr",
  3: "acegilmprstv",
  4: "acdeiklmnopqtuv",
  5: "adempv",
  6: "ert",
  7: "acdefinprsuv",
  8: "ptv",
  9: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

