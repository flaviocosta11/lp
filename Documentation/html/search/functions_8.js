var searchData=
[
  ['readfloat_152',['readFloat',['../functions_8c.html#a9e47ea299760de889ea5b98287310f1b',1,'functions.c']]],
  ['readfloatverifica_153',['readFloatVerifica',['../functions_8c.html#a7c136c79d2f353f91a004ea7df5a2985',1,'functions.c']]],
  ['readint_154',['ReadInt',['../functions_8c.html#a1aae3939dacf653c29a1647915577ed2',1,'ReadInt(int *var, int min, int max, char msg[200]):&#160;functions.c'],['../functions_8h.html#a5e11952b6420fa0de1bba0cc845c8b7d',1,'ReadInt(int *, int, int, char[]):&#160;functions.h']]],
  ['readintverifica_155',['ReadIntVerifica',['../functions_8c.html#a21042bf4afeebd49d2a8c37543b15e81',1,'functions.c']]],
  ['readstring_156',['ReadString',['../functions_8c.html#a7b9215b764e3def8ef83287da917ff64',1,'ReadString(char *var, int size, char msg[200]):&#160;functions.c'],['../functions_8h.html#ae1f25f0ef57b932faf96b0063657b6d6',1,'ReadString(char *, int, char[]):&#160;functions.h']]],
  ['reativaruser_157',['reativarUser',['../functions_8c.html#ae96f511f8edbc01bca39deccedbd3164',1,'reativarUser(pessoa *arr, int cpess):&#160;functions.c'],['../functions_8h.html#a6efa8f2be6faea4ba1900060258f7faa',1,'reativarUser(pessoa *, int):&#160;functions.c']]],
  ['redimensiona_158',['redimensiona',['../functions_8c.html#a91fe7cbc85f23443adec9ee61e95fb5e',1,'redimensiona(pessoa **cliente, int *tam):&#160;functions.c'],['../functions_8h.html#a850b5653726a77eb8d662def5462f7cb',1,'redimensiona(pessoa **cliente, int *):&#160;functions.c']]],
  ['removeencomenda_159',['removeEncomenda',['../functions_8c.html#a15ea31a4b1270ea384e255daa45b26a3',1,'removeEncomenda(struct Encomenda **encomendas, int *quantos, int *tam_max, int id):&#160;functions.c'],['../functions_8h.html#a15ea31a4b1270ea384e255daa45b26a3',1,'removeEncomenda(struct Encomenda **encomendas, int *quantos, int *tam_max, int id):&#160;functions.c']]]
];
