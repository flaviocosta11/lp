var searchData=
[
  ['tabelacusto_95',['TabelaCusto',['../functions_8c.html#acd2151770432aa6e6ef7ebe1315793cd',1,'TabelaCusto(float *precos, enum tipo_transporte transporte):&#160;functions.c'],['../functions_8h.html#a088f4ef3dafa6aabd7b787f5d0bc4719',1,'TabelaCusto(float *, enum tipo_transporte):&#160;functions.c']]],
  ['tam_96',['TAM',['../functions_8h.html#ae0b4816fb45161ef9da5e6d6134ee28a',1,'functions.h']]],
  ['tam_5fdecremento_97',['TAM_DECREMENTO',['../functions_8h.html#a4ae4d812904843b806f274aa63710fa0',1,'functions.h']]],
  ['tam_5fincremento_98',['TAM_INCREMENTO',['../functions_8h.html#a53ad4091711365bfe2802d82bbf1de5d',1,'functions.h']]],
  ['tam_5finicial_99',['TAM_INICIAL',['../functions_8h.html#afe6861715594da83296ce5c43b886013',1,'functions.h']]],
  ['tam_5fstr_100',['TAM_STR',['../functions_8h.html#ae4c997214b107810e8b9c26974af9f97',1,'functions.h']]],
  ['tipo_5fentrega_101',['tipo_entrega',['../functions_8h.html#a1af25a2fe79ae92f0310999c14978a1f',1,'functions.h']]],
  ['tipo_5ftransporte_102',['tipo_transporte',['../functions_8h.html#a814d71ae0f257a0747f8f94fedfbf0cf',1,'functions.h']]],
  ['tipo_5futilizador_103',['Tipo_Utilizador',['../functions_8h.html#a809202e2f691b492476c574d67ba5da1',1,'functions.h']]],
  ['top_5fclientes_104',['top_clientes',['../functions_8c.html#afb74486711a4204947e4c4c475f9658f',1,'top_clientes(int cpess, pessoa *arr):&#160;functions.c'],['../functions_8h.html#a706fdb47aac2bfd816b66e35eb760404',1,'top_clientes(int, pessoa *):&#160;functions.c']]],
  ['tratamento_105',['tratamento',['../struct_artigo.html#a6868e548d7455d888df4c991ab856e59',1,'Artigo']]]
];
