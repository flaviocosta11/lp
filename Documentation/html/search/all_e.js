var searchData=
[
  ['readme_81',['README',['../md__r_e_a_d_m_e.html',1,'']]],
  ['readfloat_82',['readFloat',['../functions_8c.html#a9e47ea299760de889ea5b98287310f1b',1,'functions.c']]],
  ['readfloatverifica_83',['readFloatVerifica',['../functions_8c.html#a7c136c79d2f353f91a004ea7df5a2985',1,'functions.c']]],
  ['readint_84',['ReadInt',['../functions_8c.html#a1aae3939dacf653c29a1647915577ed2',1,'ReadInt(int *var, int min, int max, char msg[200]):&#160;functions.c'],['../functions_8h.html#a5e11952b6420fa0de1bba0cc845c8b7d',1,'ReadInt(int *, int, int, char[]):&#160;functions.h']]],
  ['readintverifica_85',['ReadIntVerifica',['../functions_8c.html#a21042bf4afeebd49d2a8c37543b15e81',1,'functions.c']]],
  ['readme_2emd_86',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['readstring_87',['ReadString',['../functions_8c.html#a7b9215b764e3def8ef83287da917ff64',1,'ReadString(char *var, int size, char msg[200]):&#160;functions.c'],['../functions_8h.html#ae1f25f0ef57b932faf96b0063657b6d6',1,'ReadString(char *, int, char[]):&#160;functions.h']]],
  ['reativaruser_88',['reativarUser',['../functions_8c.html#ae96f511f8edbc01bca39deccedbd3164',1,'reativarUser(pessoa *arr, int cpess):&#160;functions.c'],['../functions_8h.html#a6efa8f2be6faea4ba1900060258f7faa',1,'reativarUser(pessoa *, int):&#160;functions.c']]],
  ['redimensiona_89',['redimensiona',['../functions_8c.html#a91fe7cbc85f23443adec9ee61e95fb5e',1,'redimensiona(pessoa **cliente, int *tam):&#160;functions.c'],['../functions_8h.html#a850b5653726a77eb8d662def5462f7cb',1,'redimensiona(pessoa **cliente, int *):&#160;functions.c']]],
  ['regiao_90',['Regiao',['../functions_8h.html#a08f6ba2c3b81eaa3c79e652afd651625',1,'functions.h']]],
  ['regular_91',['regular',['../functions_8h.html#a814d71ae0f257a0747f8f94fedfbf0cfa040a9ec6d2966f099fd3214b8ac4fef3',1,'functions.h']]],
  ['removeencomenda_92',['removeEncomenda',['../functions_8c.html#a15ea31a4b1270ea384e255daa45b26a3',1,'removeEncomenda(struct Encomenda **encomendas, int *quantos, int *tam_max, int id):&#160;functions.c'],['../functions_8h.html#a15ea31a4b1270ea384e255daa45b26a3',1,'removeEncomenda(struct Encomenda **encomendas, int *quantos, int *tam_max, int id):&#160;functions.c']]]
];
