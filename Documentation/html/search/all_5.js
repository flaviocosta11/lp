var searchData=
[
  ['gerarfatura_44',['gerarFatura',['../functions_8c.html#abbb6867e218da76c2ea26711cc4333fa',1,'gerarFatura(pessoa *pess, encomenda *encomendas, int quantos):&#160;functions.c'],['../functions_8h.html#aad8ad8132686e2b8e47f9e04b01f5029',1,'gerarFatura(pessoa *, encomenda *, int quantos):&#160;functions.c']]],
  ['getatualuser_45',['getAtualUser',['../functions_8c.html#a1e0e618a6a7c68d602d80bb56027bffc',1,'getAtualUser(pessoa *pessoas, unsigned int npessoas, unsigned int nif):&#160;functions.c'],['../functions_8h.html#a4d478ff89c29acd9a4a690f495e04f7f',1,'getAtualUser(pessoa *, unsigned int, unsigned int):&#160;functions.c']]],
  ['getencomenda_46',['getEncomenda',['../functions_8c.html#a654bad0788483d137f875429b29702a3',1,'getEncomenda(struct Encomenda *encomendas, int quantos, int id):&#160;functions.c'],['../functions_8h.html#a654bad0788483d137f875429b29702a3',1,'getEncomenda(struct Encomenda *encomendas, int quantos, int id):&#160;functions.c']]],
  ['getencomendasuser_47',['getEncomendasUser',['../functions_8c.html#afa9d79806dcedb5b4c9bd303b0fdecf4',1,'getEncomendasUser(pessoa *pess, encomenda *encomendas, int mostrarDetalhesEnc):&#160;functions.c'],['../functions_8h.html#a6fbae1c0217fe41302473d3c5ae1f335',1,'getEncomendasUser(pessoa *, encomenda *, int):&#160;functions.c']]],
  ['getenctotaisano_48',['getEncTotaisAno',['../functions_8c.html#a0f3b743db64ce51889b1feb803f67930',1,'functions.c']]],
  ['getenctotaismes_49',['getEncTotaisMes',['../functions_8c.html#a40e7bf11eb41cf5c259bf5f495c579b2',1,'functions.c']]],
  ['gettabelafator_50',['getTabelaFator',['../functions_8c.html#ad48ef416fcc81b8a69071b2ba5c68ee1',1,'functions.c']]],
  ['gettopcodpostaisdestino_51',['getTopCodPostaisDestino',['../functions_8c.html#a2999786e985000c2813b3b4171252e1c',1,'functions.c']]]
];
