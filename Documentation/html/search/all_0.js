var searchData=
[
  ['adicionaencomenda_0',['adicionaEncomenda',['../functions_8h.html#a952e877affc8418f0de098204562a850',1,'functions.h']]],
  ['adicionaencomendav2_1',['adicionaEncomendaV2',['../functions_8c.html#a15490b78ac07ec2ec5748ec6e4f14ed6',1,'adicionaEncomendaV2(struct Encomenda **encomendas, int *conta_enc, int *tam_max, float *TabelaPrecos, pessoa *cliente):&#160;functions.c'],['../functions_8h.html#a15490b78ac07ec2ec5748ec6e4f14ed6',1,'adicionaEncomendaV2(struct Encomenda **encomendas, int *conta_enc, int *tam_max, float *TabelaPrecos, pessoa *cliente):&#160;functions.c']]],
  ['alocaencomendas_2',['alocaEncomendas',['../functions_8c.html#a0854c44bd70c0bd06578d446a16a523c',1,'alocaEncomendas(encomenda **encomendas, int *tam_max):&#160;functions.c'],['../functions_8h.html#a5d76c23e0b2819678029498d11c82a96',1,'alocaEncomendas(encomenda **, int *):&#160;functions.c']]],
  ['alocapessoas_3',['alocaPessoas',['../functions_8c.html#a4b3ef0829b6d34fcfbe4ebf7e1c0a43d',1,'alocaPessoas(pessoa **pessoas, int *tam_max):&#160;functions.c'],['../functions_8h.html#add8dc8e2a4388b6647726c0f2fb3d846',1,'alocaPessoas(pessoa **, int *):&#160;functions.c']]],
  ['alocatabelaprecos_4',['AlocaTabelaPrecos',['../functions_8c.html#a0ec2fe1cbdb3d6bae78c7ffd8edeebb1',1,'AlocaTabelaPrecos():&#160;functions.c'],['../functions_8h.html#a0ec2fe1cbdb3d6bae78c7ffd8edeebb1',1,'AlocaTabelaPrecos():&#160;functions.c']]],
  ['altera_5',['altera',['../functions_8c.html#a71e2ddcd049b683d753216b4aa6a7331',1,'functions.c']]],
  ['altera_5fcliente_6',['altera_cliente',['../functions_8c.html#ae9050eeec29e71139e6fb3e6f1eaeffb',1,'altera_cliente(pessoa *arr):&#160;functions.c'],['../functions_8h.html#ae9050eeec29e71139e6fb3e6f1eaeffb',1,'altera_cliente(pessoa *arr):&#160;functions.c']]],
  ['altura_7',['altura',['../struct_volume.html#aa1a446051ec05e825038993de4e99d01',1,'Volume']]],
  ['apaga_5fcliente_8',['apaga_cliente',['../functions_8c.html#a7e24f1641587a005784cce7cc35242fe',1,'apaga_cliente(pessoa *arr):&#160;functions.c'],['../functions_8h.html#a7e24f1641587a005784cce7cc35242fe',1,'apaga_cliente(pessoa *arr):&#160;functions.c']]],
  ['apagar_5fdiretor_9',['apagar_diretor',['../functions_8c.html#a0bdc7f30042cc67fcce3debd910abc15',1,'apagar_diretor(int *cpess, pessoa *arr):&#160;functions.c'],['../functions_8h.html#a0bdc7f30042cc67fcce3debd910abc15',1,'apagar_diretor(int *cpess, pessoa *arr):&#160;functions.c']]],
  ['artigo_10',['Artigo',['../struct_artigo.html',1,'Artigo'],['../functions_8h.html#a897a4f09b74e7d81d2a4f640c610bf0a',1,'artigo():&#160;functions.h']]],
  ['artigos_11',['artigos',['../struct_encomenda.html#a9a43f4c3d3efd48211b15866fd22092a',1,'Encomenda']]],
  ['ativo_12',['ativo',['../functions_8h.html#a9d4630b1e0c310d9b8c562abd8114c49aaf9a42cf769aeaba1846b2df18d17880',1,'functions.h']]],
  ['atualizaencomenda_13',['atualizaEncomenda',['../functions_8c.html#a7e936d90089e95d31ef22bc1eaae22bd',1,'atualizaEncomenda(struct Encomenda *encomendas, int quantos, int id):&#160;functions.c'],['../functions_8h.html#a7e936d90089e95d31ef22bc1eaae22bd',1,'atualizaEncomenda(struct Encomenda *encomendas, int quantos, int id):&#160;functions.c']]]
];
