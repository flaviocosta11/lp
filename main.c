/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Authors: Flavio Pedro's
 *
 * Created on 6 de Dezembro de 2019, 18:21
 */

// quantas encomendas por més
// top clientes com mais encomendas

#include "functions.h"

int main(int argc, char** argv) {


    float *TabelaPrecos = AlocaTabelaPrecos();

    int op, op1, op2, op3, op4, op5;
    struct Encomenda *encomendas = NULL;
    struct Artigo *artigos = NULL;
    int num_artigos = 0;
    int tam_max = TAM_INICIAL;
    int id;
    int ct_pess = 1;
    int ct_encomendas = 0;
    int ct_soc = 0;

    int ct_pessoas_max = TAM_INICIAL;
    int posicao_login_user = 0;

    artigo *atigos;
    pessoa *pesso;
    morada morad;


    //encomendas = (encomenda*) malloc(TAM_INICIAL * sizeof (encomenda));

    if (alocaPessoas(&pesso, &ct_pessoas_max) && alocaEncomendas(&encomendas, &tam_max)) {
        free(pesso);
        free(encomendas);
    } else {
        printf("erro a alocar memoria!");
    }

    loadDados("default_cliente.bin", &pesso, &ct_pess, &encomendas, &ct_encomendas, &TabelaPrecos);
    printf("/nMAIN - NIF TESTE - %d\n\n", pesso[0].nif);

    int user;
    int nif;
    int login = 0;


    printf("0 - SAIR");
    printf("\n1 - Cliente");
    printf("\n2 - Diretor");
    ReadInt(&user, 0, 2, "\nIntroduza o numero da opcao que deseja usar: ");

    switch (user) {
        case 0:
            break;
        case 1:
            do {
                ReadInt(&nif, 100000000, 999999999, "\n\n*****  Login  *****\n\nIntroduza o seu nif:\n");
                posicao_login_user = verifica_utilizador(ct_pess, pesso, nif);
                if (posicao_login_user != -1) {
                    //printf("\n %d \n", (pesso + posicao_login_user)->nif);
                    login = 1;
                } else {
                    int reg = 0;
                    ReadInt(&reg, 1, 2, "Deseja se registar? \nIntroduza '1' para sim ou '2' para nao: ");
                    if (reg == 1) {
                        introduzir_cliente(&ct_pess, pesso, &ct_pessoas_max, nif);
                        //saveAll("default_cliente.bin", pesso, encomendas, ct_pess, ct_encomendas,TabelaPrecos);
                    } else {
                        login = 3;
                    }
                }

            } while (login == 0 && login != 3);

            if (login != 3) {

                pessoa *cliente = getAtualUser(pesso, ct_pess, nif);

                if (cliente->estado == ativo) {
                    do {
                        printf("\n0 - SAIR");
                        printf("\n1 - Gestão de Perfil");
                        printf("\n2 - Gestão de encomendas");
                        printf("\n3 - Ver tabela de preços");

                        ReadInt(&op, 0, 12, "\nIntroduza o numero da opcao que deseja usar:");

                        switch (op) {
                            case 1:
                                do {
                                    printf("\n0 - Menu Anterior");
                                    printf("\n1 - Editar Perfil");
                                    printf("\n2 - Remover Perfil");
                                    ReadInt(&op1, 0, 100, "\nIntrdoduza a opção que deseja: ");

                                    switch (op1) {

                                        case 1: altera_cliente(&cliente);

                                            break;
                                        case 2:if (apaga_cliente(&cliente) == 1) {
                                                printf("A sua conta foi desativa, para reativar contactar o diretor");
                                                return (EXIT_SUCCESS);
                                            }
                                            break;
                                    }

                                } while (op1 != 0);

                                break;
                            case 2:

                                do {
                                    printf("\n1 - Menu Anterior");
                                    printf("\n2 - Acrescentar nova Encomenda para transporte");
                                    printf("\n3 - Consultar Estado Encomenda");
                                    printf("\n4 - Alterar Encomenda");
                                    printf("\n5 - Cancelar Encomenda");
                                    printf("\n6 - Ver minhas encomendas");
                                    ReadInt(&op2, 0, 100, "\nIntrdoduza a opção que deseja: ");
                                    switch (op2) {

                                        case 2:
                                            //adicionaEncomenda(&encomendas, &ct_encomendas, &tam_max, TabelaPrecos, getAtualUser(pesso, ct_pess, nif));
                                            //saveAll("default_cliente.bin", pesso, encomendas, ct_pess, ct_encomendas,TabelaPrecos);
                                            adicionaEncomendaV2(&encomendas, &ct_encomendas, &tam_max, TabelaPrecos, getAtualUser(pesso, ct_pess, nif));
                                            break;
                                        case 3:
                                            listarEncomendas(encomendas, ct_encomendas);

                                            break;
                                        case 4:
                                            ReadInt(&id, 1, 9999, "Introduza o id da encomenda que pertende atualizar: ");
                                            atualizaEncomenda(encomendas, ct_encomendas, id);
                                            break;
                                        case 5:
                                            ReadInt(&id, 1, 9999, "Introduza o id da encomenda que pertende remover: ");
                                            //removeEncomenda(&encomendas, &quantos, &tam_max, id);
                                            cancelarEncomenda(encomendas, ct_encomendas, id);
                                            //removeEncomenda(&encomendas, &ct_encomendas, &tam_max, id);
                                            break;
                                        case 6:
                                            //getEncomendasUser(cliente, encomendas,1);
                                            gerarFatura(cliente, encomendas, ct_encomendas);
                                            break;

                                    }
                                } while (op2 != 1);


                                break;
                            case 3:
                                printTabelaPrecos(TabelaPrecos);
                                printf("\n\n");
                                break;
                                break;
                        }


                    } while (op != 0);
                } else {
                    printf("A sua conta encontra-se desativa!\nPara ativar novamente contacte administrador.\n\n");
                }
            }
            break;
        case 2:
            do {
                printf("0 - SAIR");
                printf("\n1 - Gestão de utilizadores");
                printf("\n2 - Gestão de encomendas");
                printf("\n3 - Gestão de preços");
                printf("\n4 - Geração de faturas");
                printf("\n5 - Persistência de dados");
                printf("\n6 - Listagens");

                
                ReadInt(&op,0,12,"\nIntroduza o numero da opcao que deseja usar: ");

                switch (op) {
                    case 1:
                        do {
                            printf("\n0 - Menu Anterior");
                            printf("\n1 - Criar Cliente");
                            printf("\n2 - Editar Cliente");
                            printf("\n3 - Remover Cliente");
                            printf("\n4 - Listar Clientes");
                            printf("\n5 - Alterar Estado de Clientes");
                            ReadInt(&op1, 0, 100, "\nIntrdoduza a opção que deseja: ");

                            switch (op1) {
                                    int nif_adm = 0;

                                case 1:
                                    ReadInt(&nif_adm, 100000000, 999999999, "\nIntroduza o nif\n");
                                    introduzir_cliente(&ct_pess, pesso, &ct_pessoas_max, nif_adm);
                                    break;
                                case 2:altera(&ct_pess, pesso);
                                    break;
                                case 3:apagar_diretor(&ct_pess,pesso);
                                    break;
                                case 4:
                                    lista_clientes(ct_pess, pesso);
                                    break;
                                case 5:
                                    reativarUser(pesso, ct_pess);
                                    break;
                               
                            }

                        } while (op1 != 0);

                        break;
                    case 2:

                        do {
                            printf("\n0 - Menu Anterior");
                            //printf("\n2 - Acrescentar nova Encomenda para transporte");
                            printf("\n1 - Listar Encomendas");
                            printf("\n2 - Atualizar estado Encomenda");
                            ReadInt(&op2, 0, 100, "\nIntrdoduza a opção que deseja: ");
                            switch (op2) {

                                case 1:
                                    listarEncomendas(encomendas, ct_encomendas);
                                    break;
                                case 2:
                                    ReadInt(&id, 1, 9999, "Introduza o id da encomenda que pertende atualizar: ");
                                    /*
                                     TODO: FALTA ALTUALIZAR ENCOMENDA PARA EXPEDIDA ETC 
                                     */
                                    //atualizaEncomenda(encomendas, quantos, id); 
                                    break;
                            }
                        } while (op2 != 0);


                        break;
                    case 3:
                        do {
                            printf("\n1 - Menu Anterior");
                            printf("\n2 - Consultar preços");
                            printf("\n3 - Criar nova Tabela Preços");
                            printf("\n4 - Editar Tabela Preços");
                            ReadInt(&op3, 0, 100, "\nIntrdoduza a opção que deseja: ");
                            switch (op3) {
                                case 2:
                                    printTabelaPrecos(TabelaPrecos);
                                    break;
                                case 3:
                                    criarTabelaPrecos(TabelaPrecos);
                                    break;
                                case 4:
                                    editarTabelaPrecos(TabelaPrecos);
                                    break;
                            }
                        } while (op3 != 1);

                        break;
                    case 4:
                        do {
                            printf("\n1 - Menu Anterior");
                            printf("\n2 - Gerar fatura");
                            printf("\n3 - Reimprimir fatura");
                            ReadInt(&op4, 0, 100, "\nIntrdoduza a opção que deseja: ");
                            switch (op4) {
                                case 2:
                                    break;
                                case 3:
                                    break;
                                case 4:
                                    break;
                            }
                        } while (op4 != 1);

                        break;
                    case 5:
                        do {
                            printf("\n1 - Menu Anterior");
                            printf("\n2 - Guardar dados");
                            printf("\n3 - Carregar Dados");
                            ReadInt(&op5, 0, 100, "\nIntrdoduza a opção que deseja: ");
                            switch (op5) {
                                case 2:
                                    //saveAll("data.bin", pesso, encomendas, ct_pess, ct_encomendas);
                                    break;
                                case 3:
                                    //loadDados("data.bin", pesso, ct_pess, encomendas, ct_encomendas);
                                    break;
                            }
                        } while (op5 != 1);

                        break;
                        
                    case 6:
                        
                        break;

                }


            } while (op != 0);
            break;
    }







    return (EXIT_SUCCESS);
}

